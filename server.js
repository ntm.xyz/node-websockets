'use strict';

const express = require('express');
const WebSocket = require('ws');
const path = require('path');
const { parseMessage } = require('./messageHelper');

const PORT = process.env.PORT || 3000;
const SocketServer = WebSocket.Server;
const INDEX = path.join(__dirname, 'index.html');

const server = express()
  .use((req, res) => res.sendFile(INDEX) )
  .listen(PORT, () => console.log(`Listening on ${ PORT }`));

const wss = new SocketServer({ server });

wss.on('connection', (ws) => {
  ws.send('CONNECTED');

  ws.on('close', () => console.log('Client disconnected'));

  ws.on('message', function incoming(data) {
    const msg = parseMessage(data);
    console.log(msg);
    if (msg.command === 'REGISTER_ID') {
      ws.registerId = msg.fromId;
      ws.send('REGISTERED');
    }
    // Broadcast to everyone else.
    wss.clients.forEach(function each(client) {
      if (client !== ws && client.readyState === WebSocket.OPEN && client.registerId === msg.toId) {
        client.send(data);
      }
    });
  });
});